﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AjaxRequest.Startup))]
namespace AjaxRequest
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
