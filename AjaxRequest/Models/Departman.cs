﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AjaxRequest.Models
{
    public class Departman
    {
        public int Id { get; set; }
        public string Ad { get; set; }
    }
}